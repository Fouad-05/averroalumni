<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<form action="addDiploma" method="post">
	
		<input id="firstname" type="text" name="titleDiploma" placeholder="Titre Diplome" required />
		<br>
		<input id="yearDiploma" type="text" name="yearDiploma" placeholder="Ann�e Obtention Diplome" required />
		<br>
		<input id="descDiploma" type="text" name="descDiploma" placeholder="Description" required />
		<br>
		<input id="linkDiploma" type="text" name="linkDiploma" placeholder="lien vers diplome" required />
		<br>
		<input id="school" type="text" name="school" placeholder="Etablissement" required />
		<br>
		<input id="levelDiploma" type="text" name="levelDiploma" placeholder="Niveau du diplome" required />
		<br>
		<input id="typeDiploma" type="text" name="typeDiploma" placeholder="Type de Diplome (universitaire, DUT...)" required />
		<br>
		<input id="idProfil" type="hidden" name="idAccount" placeholder="" required value="${account.idAccount}"/>
		
	
		<button type="submit" >VALIDER</button>
	</form>
	
</body>
</html>