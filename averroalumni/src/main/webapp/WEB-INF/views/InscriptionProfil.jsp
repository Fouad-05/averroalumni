<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<title>Inscription Profil</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://unpkg.com/tailwindcss@2.2.4/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
	<div class="grid min-h-screen place-items-center">
	  <div class="w-11/12 p-12 bg-white sm:w-8/12 md:w-1/2 lg:w-5/12">
	    <h1 class="text-xl font-semibold">Hello there, <span class="font-normal">please fill in your information to continue</span></h1>
	    <form class="mt-6" method="post" action="signUp">
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="firstname" class="block text-xs font-semibold text-gray-600 uppercase">NOM *</label>
	          <input id="firstname" type="text" name="nom" placeholder="Nom" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	        </span>
	        <span class="w-1/2">
	          <label for="lastname" class="block text-xs font-semibold text-gray-600 uppercase">PRENOM *</label>
	        <input id="lastname" type="text" name="prenom" placeholder="Prenom" autocomplete="family-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	        </span>
	      </div>
	      <br>
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="phoneNumber" class="block text-xs font-semibold text-gray-600 uppercase">Numero de telephone</label>
	          <input id="phoneNumber" type="tel" name="phoneNumber" placeholder="XX XX XX XX XX" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" />
	        </span>
	        <span class="w-1/2">
	          <label for="yearBac" class="block text-xs font-semibold text-gray-600 uppercase">ANNEE OBTENTION BAC *</label>
	        	<select id="yearBac" name="yearBac" placeholder="Annee d'obtention du bac" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required>
		        	<option>-</option>
		        	<option>2006</option>
		        	<option>2007</option>
		        	<option>2008</option>
		        	<option>2009</option>
		        	<option>2010</option>
		        	<option>2011</option>
		        	<option>2012</option>
		        	<option>2013</option>
		        	<option>2014</option>
		        	<option>2015</option>
		        	<option>2016</option>
		        	<option>2017</option>
		        	<option>2018</option>
		        	<option>2019</option>
		        	<option>2020</option>
	        		<option>2021</option>
	        	
	        	</select>
	        </span>
	      </div>
	      <label for="email" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">E-mail *</label>
	      <input id="email" type="email" name="mail" placeholder="mail" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	      
	      <label for="jobTitle" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Profession *</label>
	      <input id="jobTitle" type="text" name="jobTitle" placeholder="Job" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	      <br>
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="activitySector" class="block text-xs font-semibold text-gray-600 uppercase">Secteur d'activite</label>
	          <input id="activitySector" type="text" name="activitySector" placeholder="Secteur d'activite" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  />
	        </span>
	        <span class="w-1/2">
	          <label for="linkJobCard" class="block text-xs font-semibold text-gray-600 uppercase">Lien fiche metier profession</label>
	        <input id="linkJobCard" type="url" name="linkJobCard" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  />
	        </span>
	      </div>
	      <br>
	      <div class="flex justify-between gap-3">
	         <span class="w-1/2">
	          	<label for="linkedin" class="block text-xs font-semibold text-gray-600 uppercase">Lien linkedin</label>
	        	<input id="linkedin" type="url" name="linkedin" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  />
	        </span>
	        <span class="w-1/2">
	          	<label for="linkCompany" class="block text-xs font-semibold text-gray-600 uppercase">Lien vers entreprise</label>
	        	<input id="linkCompany" type="url" name="linkCompany" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  />
	        </span>
	      </div>
	      <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Mot de passe *</label>
	      <input id="password" type="password" name="pwd" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	      <label for="password-confirm" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Confirmer mot de passe *</label>
	      <input id="password-confirm" type="password" name="password-confirm" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
	      <button type="submit" class="w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-black shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
	        S'inscrire
	      </button>
	      <p class="flex justify-between inline-block mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Champs obligatoire *</p>
	      <p class="flex justify-between inline-block mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Already registered?</p>
	    </form>
	  </div>
	</div>

</body>
</html>