<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://unpkg.com/tailwindcss@2.0.2/dist/tailwind.min.css" rel="stylesheet">
<title>Averroalumni</title>
</head>
<body class="h-screen overflow-hidden flex  justify-center" style="background: #008080;" id="main">

    <div class="overflow-x-auto">
        <div class="min-w-screen h-screen bg-green-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
            <div class="w-full lg:w-6/6">
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                            <tr class="bg-blue-100 text-gray-600 uppercase text-sm leading-normal">
                                <th class="py-3 px-6 text-left">Project</th>
                                <th class="py-3 px-6 text-left">Client</th>
                                <th class="py-3 px-6 text-center">Promo</th>
                                <th class="py-3 px-6 text-center">Status</th>
                                <th class="py-3 px-6 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                        
                        	<c:forEach items="${ listProfil }" var="Profil">
	                            <tr class="border-b border-gray-200 hover:bg-gray-100">
	                                <td class="py-3 px-6 text-left whitespace-nowrap">
	                                    <div class="flex items-center">
	                                        <span class="font-medium"> ${ Profil.getInformationProfil().getJobTitle() } </span>
	                                      	<script type="text/javascript">console.log("${ Profil.dateCreationCompte }")</script>
	                                    </div>
	                                </td>
	                                <td class="py-3 px-6 text-left">
	                                    <div class="flex items-center">
	                                        <div class="mr-2">
	                                            <img class="w-6 h-6 rounded-full" src="assets/images/fouad_Linkedin.png"/>
	                                        </div>
	                                        <span>${ Profil.nom } ${ Profil.prenom } </span>
	                                    </div>
	                                </td>
	                                <td class="py-3 px-6 text-center">
	                                  <div class="flex items-center">
	                                      <span class="font-medium">${ Profil.yearBac }</span>
	                                  </div>
	                                </td>
	                                
	                                <td class="py-3 px-6 text-center">
	                                    <span class="bg-purple-200 text-purple-600 py-1 px-3 rounded-full text-xs">${ Profil.studentClass }</span>
	                                </td>
	                                
	                                
	                                <td class="py-3 px-6 text-center">
	                                    <div class="flex item-center justify-center">
	                                        <div class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
	                                        <form action="viewProfil" method="post" class="profilView">
	                                        	<input id="" type="hidden" class="" required value="${ Profil.idProfil }" name="idProfil"/>
	                                        	<input id="" type="hidden" class="" required value="${ Profil.idProfil }" name="idAccount"/>
	                                        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
		                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
		                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
		                                            </svg>
	                                        </form>
	                                        </div>
	                                        <div class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
	                                        <form action="editProfilRedirection" method="post" class="formEdit">
	                                        	<input id="" type="hidden" class="" required value="${ Profil.idProfil }" name="idProfil"/>
	                                        		<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
		                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
		                                            </svg>
	                                        	
	                                        	<%-- <a href="editProfilRedirection?idProfil=${ Profil.idProfil }">
		                                            
	                                            </a> --%>
	                                        </form>
	                                        	
	                                        </div>
	                                        <div class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
	                                        	<a href="#" class="suppression" value="${Profil.idProfil }" value2="${Profil.idProfil }">
		                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
		                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
		                                            </svg>
	                                            </a>
	                                        </div>
	                                    </div>
	                                </td>
	                            </tr>
	                    	</c:forEach>
	                    	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" id="testScriptMan">



      $( "#delete" ).click(function() {
        var href = $(this).attr('href');
        //$("body").remove("#mainPopUp");

        console.log("href");

      })
      $( ".cancel" ).click(function() {
        console.log("CANCEL");

      })

		
      $( ".formEdit" ).click(function() {
        $(this).submit();

      })
      
       $( ".profilView" ).click(function() {
        $(this).submit();

      })
      
      $( ".suppression" ).click(function() {

        var info = $(this).attr('value');
        var info2 = $(this).attr('value2');
        console.log(info + info2);

        var chbx='<div id="mainPopUp" class="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover"  style="background-color:rgba(255,0,0,0.1);" id="modal-id"><div class="absolute bg-black opacity-80 inset-0 z-0"></div><div class="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white "><div class=""><div class="text-center p-5 flex-auto justify-center"><svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 -m-1 flex items-center text-red-500 mx-auto" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><svg xmlns="http://www.w3.org/2000/svg" class="w-16 h-16 flex items-center text-red-500 mx-auto" viewBox="0 0 20 20" fill="currentColor"><path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" /></svg><h2 class="text-xl font-bold py-4 ">Etes vous sur?</h3><p class="text-sm text-gray-500 px-8">Voulez vous vraiment supprimer ce compte ? Ce proceder sera definitif</p></div><div class="p-3  mt-2 text-center space-x-4 md:block"><button class="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100 cancel" id="cancel">CANCEL</button><button class="mb-2 md:mb-0 bg-red-500 border border-red-500 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-red-600"><a href="suppressionProfil?idAccount='+info+'&idProfil='+info2+'" id="delete" value="suppressionProfil?idAccount="'+info+'"&idProfil="'+info2+'"">DELETE</a></button></div></div></div></div>';

        //var testScript ='$( ".cancel" ).click(function() {$("#mainPopUp").remove(); console.log("1");})';




        console.log(info, info2);



          $("#main").prepend(chbx);
          console.log(chbx);
          console.log($("#cancel").click(function() {$("#mainPopUp").remove();}));
          console.log($( "#delete" ).click(function() {
            
            //$("body").remove("#mainPopUp");
          }));
          //$("#testScriptMan").append(testScript);



      })

    </script>
</body>
</html>