<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<title>Inscription Profil</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://unpkg.com/tailwindcss@2.2.4/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
	<div class="grid min-h-screen place-items-center">
	  <div class="w-11/12 p-12 bg-white sm:w-8/12 md:w-1/2 lg:w-5/12">
	    <h1 class="text-xl font-semibold">Hello there, <span class="font-normal">please fill in your information to continue</span></h1>
	    <form class="mt-6" method="post" action="editProfilPro">
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="firstname" class="block text-xs font-semibold text-gray-600 uppercase">NOM *</label>
	          <input id="firstname" type="text" name="nom" placeholder="Nom" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.nom }"/>
	        </span>
	        <span class="w-1/2">
	          <label for="lastname" class="block text-xs font-semibold text-gray-600 uppercase">PRENOM *</label>
	        <input id="lastname" type="text" name="prenom" placeholder="Prenom" autocomplete="family-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.prenom }" readonly="readonly"/>
	        </span>
	      </div>
	      <br>
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="phoneNumber" class="block text-xs font-semibold text-gray-600 uppercase">Numero de telephone</label>
	          <input id="phoneNumber" type="tel" name="phoneNumber" placeholder="XX XX XX XX XX" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner " value="${ Profil.phoneNumber }" />
	        </span>
	        <span class="w-1/2">
	          <label for="yearBac" class="block text-xs font-semibold text-gray-600 uppercase">ANNEE OBTENTION BAC *</label>
	          <input id="yearBac" type="text" name="yearBac" placeholder="XX XX XX XX XX" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" value="${ Profil.yearBac }" readonly/>
	        </span>
	      </div>
	      <label for="email" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">E-mail *</label>
	      <input id="email" type="email" name="mail" placeholder="mail" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.mail }"/>
	      
	      <label for="jobTitle" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Profession *</label>
	      <input id="jobTitle" type="text" name="jobTitle" placeholder="Job" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.getInformationProfil().getJobTitle() }"/>
	      <br>
	      <div class="flex justify-between gap-3">
	        <span class="w-1/2">
	          <label for="activitySector" class="block text-xs font-semibold text-gray-600 uppercase">Secteur d'activite</label>
	          <input id="activitySector" type="text" name="activitySector" placeholder="Secteur d'activite" autocomplete="given-name" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" value="${ Profil.getInformationProfil().getActivitySector() }" />
	        </span>
	        <span class="w-1/2">
	          <label for="linkJobCard" class="block text-xs font-semibold text-gray-600 uppercase">Lien fiche metier profession</label>
	        <input id="linkJobCard" type="url" name="linkJobCard" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  value="${ Profil.getInformationProfil().getLinkJobCard() }"/>
	        </span>
	      </div>
	      <br>
	      <div class="flex justify-between gap-3">
	         <span class="w-1/2">
	          	<label for="linkedin" class="block text-xs font-semibold text-gray-600 uppercase">Lien linkedin</label>
	        	<input id="linkedin" type="url" name="linkedin" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  value="${ Profil.getInformationProfil().getLinkedin() }"/>
	        </span>
	        <span class="w-1/2">
	          	<label for="linkCompany" class="block text-xs font-semibold text-gray-600 uppercase">Lien vers entreprise</label>
	        	<input id="linkCompany" type="url" name="linkCompany" placeholder="https://example.com" autocomplete="family-name" pattern="https://.*" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"  value="${ Profil.getInformationProfil().getLinkCompany() }"/>
	        </span>
	      </div>
	      <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Mot de passe *</label>
	      <input id="password" type="password" name="pwd" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.getAccount().getPwd() }"/>
	      <label for="password-confirm" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Confirmer mot de passe *</label>
	      <input id="password-confirm" type="password" name="password-confirm" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required value="${ Profil.getAccount().getPwd() }"/>
	      <input id="" type="hidden" class="" required value="${ Profil.idProfil }" name="idProfil"/>

	      <button type="submit" class="w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-black shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
	        S'inscrire
	      </button>
	      <p class="flex justify-between inline-block mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Champs obligatoire *</p>
	    </form>
	  </div>
	</div>

</body>
</html>