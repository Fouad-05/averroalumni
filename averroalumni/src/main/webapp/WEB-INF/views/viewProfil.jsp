<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Averroalumni</title>
<link href="https://unpkg.com/tailwindcss@2.2.4/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
	<p>${Profil.idProfil }</p>
	<p>${Profil.nom }</p>
	<p>${Profil.prenom }</p>
	<p>${Profil.mail }</p>
	<c:if test="${Profil.phoneNumber  != null}">
		<p>${Profil.phoneNumber}</p>
	</c:if>
	<p>${Profil.yearBac }</p>
	<p></p>
	
	<div class="min-w-max" style="width : 80%;">
      <!-- INPUT -->


      <!-- INPUT -->
      <ul class="rounded-md shadow-md bg-white m-full left-0 right-0 -bottom-18 mt-3 p-3">
          <li class="text-xs uppercase text-gray-400 border-b border-gray border-solid py-2 px-5 mb-2">
              DIPLOMES
          </li>
          <li class="grid grid-cols-10 gap-4 justify-center items-center cursor-pointer px-4 py-2 rounded-lg hover:bg-gray-50">
              <div class="flex justify-center items-center">
                <div class="h-6 w-6">
                  <img src="paper.png" alt="">
                </div>

              </div>
              <c:forEach items="${ listDiplomeProfil }" var="Diplome">
              
              <div class="col-start-2 col-end-11 pl-8 border-l-2 border-solid border-gray">
                  <h3 class="text-gray-900 font-medium text-md">${Diplome.titleDiploma }</h3>
                  <p class="text-gray-600 mt-1 font-regular text-sm">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vehicula gravida justo, at auctor lorem rutrum at.
                  </p>
                  <div class="inline-block mr-2 mt-2">
                      <button type="button" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg">
                          <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
                          </svg>
                      </button>
                      <button type="button" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-gray-500 hover:bg-gray-600 hover:shadow-lg">
                          <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                          </svg>
                      </button>
                  </div>
              </div>
              </c:forEach>
          </li>
          <div class="inline-block mr-2 mt-2">
	          <form action="viewAddDiplome" method="post" id="addDiplome">
	          	<input id="idProfil" type="hidden" name="idProfil" placeholder="Titre Diplome" required value="${ Profil.idProfil }"/>
	          	<button type="button" class="focus:outline-none text-white text-xs py-2 px-4 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">AJOUTER DIPLOME</button>
	          
	          </form>
          </div>

          <li class="text-xs uppercase text-gray-400 border-b border-gray border-solid py-2 px-5 mb-2">
              COMPETENCES
          </li>
          <li class="grid grid-cols-10 gap-4 justify-center items-center cursor-pointer px-4 py-2 rounded-lg hover:bg-gray-50">
              <div class="flex justify-center items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-yellow-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19.428 15.428a2 2 0 00-1.022-.547l-2.387-.477a6 6 0 00-3.86.517l-.318.158a6 6 0 01-3.86.517L6.05 15.21a2 2 0 00-1.806.547M8 4h8l-1 1v5.172a2 2 0 00.586 1.414l5 5c1.26 1.26.367 3.414-1.415 3.414H4.828c-1.782 0-2.674-2.154-1.414-3.414l5-5A2 2 0 009 10.172V5L8 4z" />
                  </svg>
              </div>
              <div class="col-start-2 col-end-11 pl-8 border-l-2 border-solid border-gray">
                  <h3 class="text-gray-900 font-medium text-md">Energy Elixir</h3>
                  <p class="text-gray-600 mt-1 font-regular text-sm">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vehicula gravida justo, at auctor lorem rutrum at.
                  </p>
                  <div class="inline-block mr-2 mt-2">
                      <button type="button" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg">
                          <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
                          </svg>
                      </button>
                      <button type="button" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-gray-500 hover:bg-gray-600 hover:shadow-lg">
                          <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                          </svg>
                      </button>
                  </div>
              </div>
          </li>
          <div class="inline-block mr-2 mt-2">
              <button type="button" class="focus:outline-none text-white text-xs py-2 px-4 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">AJOUTER DIPLOME</button>
          </div>
      </ul>
  </div>
  <c:forEach items="${ listDiplome }" var="Diplome">
  <p>DIPLOME PROFIL ID : ${Diplome.getProfil().getIdProfil() }</p>
  </c:forEach>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
    
    $( "#addDiplome" ).click(function() {
        $(this).submit();

      })
    </script>

</body>
</html>