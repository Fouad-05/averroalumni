package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.CandidacyCenter;

public interface CandidacyCenterRepository extends JpaRepository<CandidacyCenter, Integer>{

}
