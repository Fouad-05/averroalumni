package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.Evenement;

public interface EventRepository extends JpaRepository<Evenement, Integer>{

}
