package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.CareerCenter;

public interface CareerCenterRepository  extends JpaRepository<CareerCenter, Integer>{

}
