package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.Profil;

public interface ProfilRepository extends JpaRepository<Profil, Integer>{

}
