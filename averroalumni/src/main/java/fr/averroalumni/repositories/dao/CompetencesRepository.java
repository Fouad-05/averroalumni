package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.Competences;

public interface CompetencesRepository extends JpaRepository<Competences, Integer>{

}
