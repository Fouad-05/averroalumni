package fr.averroalumni.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.averroalumni.beans.Diploma;

public interface DiplomaRepository extends JpaRepository<Diploma, Integer>{

}
