package fr.averroalumni.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.averroalumni.beans.CareerCenter;
import fr.averroalumni.beans.Profil;
import fr.averroalumni.repositories.dao.CareerCenterRepository;
import fr.averroalumni.repositories.dao.ProfilRepository;

@Controller
public class CareerCenterController {

	@Autowired
	CareerCenterRepository ccr;
	
	@Autowired
	ProfilRepository pr;
	
	@PostMapping(value = "/redirectFormCreateCareerCenter")
	public ModelAndView redirectFormCreateCareerCenter(ModelAndView mv, @RequestParam(value = "idProfil") Integer idProfil) {
		
		Profil profil = pr.findById(idProfil).get();

		mv.addObject("profil", profil);
		
		mv.setViewName("createCareerCenter");
		
		return mv;
	}
	
	@PostMapping(value = "/createCareerCenter")
	public ModelAndView createCareerCenter(ModelAndView mv, @RequestParam(value = "idProfil") Integer idProfil, 
			
			@RequestParam(value = "typeAnnonce") String typeAnnonce,
			@RequestParam(value = "descAnnonce") String descAnnonce, 
			@RequestParam(value = "levelAnnonce") String levelAnnonce,
			@RequestParam(value = "durationAnnonce") String durationAnnonce,
			@RequestParam(value = "contactAnnonce") String contactAnnonce, 
			@RequestParam(value = "companyAnnonce") String companyAnnonce,
			@RequestParam(value = "rythmeAnnonce") String rythmeAnnonce,
			@RequestParam(value = "dateStartAnnonce") String dateStartAnnonce, 
			@RequestParam(value = "sectorAnnonce") String sectorAnnonce,
			@RequestParam(value = "pdf") String pdf) {

		LocalDate dateAnnonce = LocalDate.now();
		LocalDate startAnnonce = LocalDate.parse(dateStartAnnonce);
		
		CareerCenter careerCenter = new CareerCenter(typeAnnonce, descAnnonce, levelAnnonce, durationAnnonce, 
				contactAnnonce, companyAnnonce, dateAnnonce, rythmeAnnonce, startAnnonce, sectorAnnonce, pdf);
		
		Profil profil = pr.findById(idProfil).get();

		careerCenter.setProfil(profil);
		
		ccr.saveAndFlush(careerCenter);
	
		pr.saveAndFlush(profil);
		
		mv.addObject("profil", profil);
		
		ArrayList<CareerCenter> listCareerCenter = (ArrayList<CareerCenter>) ccr.findAll();
		
		ArrayList<CareerCenter> myListCareerCenter = new ArrayList<CareerCenter>();
		
		for (int i = 0; i < listCareerCenter.size() ; i++) {
			
			CareerCenter careerCenterTemp = listCareerCenter.get(i);
			
			if (careerCenterTemp.getProfil().getIdProfil() == idProfil) {
				
				myListCareerCenter.add(careerCenterTemp);
				
			}	
		}
		
		mv.addObject("myListCareerCenter", myListCareerCenter);

		mv.setViewName("myCareerCenter");

		return mv;
	}
	
	@PostMapping(value = "/redirectUpdateCareerCenter")
	public ModelAndView redirectUpdateCareerCenter(ModelAndView mv, 
			
			@RequestParam(value = "idCareerCenter") Integer idCareerCenter, 
			@RequestParam(value = "idProfil") Integer idProfil) {
		
		Profil profil = pr.findById(idProfil).get();
		
		CareerCenter careerCenter = ccr.findById(idCareerCenter).get();

		mv.addObject("profil", profil);
		
		mv.addObject("careerCenter", careerCenter);
		
		mv.setViewName("updateCareerCenter");
		
		return mv;
	}
	
	@PostMapping(value = "/updateCareerCenter")
	public ModelAndView updateCareerCenter(ModelAndView mv, 
			
			@RequestParam(value = "idProfil") Integer idProfil,
			@RequestParam(value = "idCareerCenter") Integer idCareerCenter, 
			
			@RequestParam(value = "typeAnnonce") String typeAnnonce,
			@RequestParam(value = "descAnnonce") String descAnnonce, 
			@RequestParam(value = "levelAnnonce") String levelAnnonce,
			@RequestParam(value = "durationAnnonce") String durationAnnonce,
			@RequestParam(value = "contactAnnonce") String contactAnnonce, 
			@RequestParam(value = "companyAnnonce") String companyAnnonce,
			@RequestParam(value = "rythmeAnnonce") String rythmeAnnonce,
			@RequestParam(value = "dateStartAnnonce") String dateStartAnnonce, 
			@RequestParam(value = "sectorAnnonce") String sectorAnnonce,
			@RequestParam(value = "pdf") String pdf) {
		
		LocalDate dateAnnonce = LocalDate.now();
		LocalDate startAnnonce = LocalDate.parse(dateStartAnnonce);
		
		CareerCenter careerCenter = new CareerCenter(typeAnnonce, descAnnonce, levelAnnonce, durationAnnonce, 
				contactAnnonce, companyAnnonce, dateAnnonce, rythmeAnnonce, startAnnonce, sectorAnnonce, pdf);
		
		Profil profil = pr.findById(idProfil).get();
		
		careerCenter.setProfil(profil);
		careerCenter.setIdCareerCenter(idCareerCenter);

		ccr.saveAndFlush(careerCenter);

		mv.addObject("profil", profil);
		
		mv.addObject("careerCenter", careerCenter);
		
		mv.setViewName("updateCareerCenter");
		
		return mv;

	}
	
	@PostMapping(value = "/deleteCareerCenter")
	public ModelAndView deleteCareerCenter(ModelAndView mv, 
			
			@RequestParam(value = "idProfil") Integer idProfil,
			@RequestParam(value = "idCareerCenter") Integer idCareerCenter) {
		
		
		ccr.deleteById(idCareerCenter);
		
		ArrayList<CareerCenter> listCareerCenter = (ArrayList<CareerCenter>) ccr.findAll();
		
		ArrayList<CareerCenter> myListCareerCenter = new ArrayList<CareerCenter>();
		
		for (int i = 0; i < listCareerCenter.size() ; i++) {
			
			CareerCenter careerCenterTemp = listCareerCenter.get(i);
			
			if (careerCenterTemp.getProfil().getIdProfil() == idProfil) {
				
				myListCareerCenter.add(careerCenterTemp);
				
			}	
		}
		
		mv.addObject("myListCareerCenter", myListCareerCenter);

		mv.setViewName("myCareerCenter");
		
		return mv;
	}
	
	@PostMapping(value = "/myListCareerCenter")
	public ModelAndView myListCareerCenter(ModelAndView mv, @RequestParam(value = "idProfil") Integer idProfil) {

		ArrayList<CareerCenter> listCareerCenter = (ArrayList<CareerCenter>) ccr.findAll();
		
		ArrayList<CareerCenter> myListCareerCenter = new ArrayList<CareerCenter>();
		
		for (int i = 0; i < listCareerCenter.size() ; i++) {
			
			CareerCenter careerCenterTemp = listCareerCenter.get(i);
			
			if (careerCenterTemp.getProfil().getIdProfil() == idProfil) {
				
				myListCareerCenter.add(careerCenterTemp);
				
			}	
		}
		
		mv.addObject("myListCareerCenter", myListCareerCenter);

		mv.setViewName("myCareerCenter");
		
		return mv;
	}
	
	@PostMapping(value = "/listCareerCenter")
	public ModelAndView listCareerCenter(ModelAndView mv, @RequestParam(value = "idProfil") Integer idProfil) {

		ArrayList<CareerCenter> listCareerCenter = (ArrayList<CareerCenter>) ccr.findAll();
		
		mv.addObject("listCareerCenter", listCareerCenter);

		mv.setViewName("listCareerCenter");
		
		return mv;
	}
	
	@PostMapping(value = "/descriptionCareerCenter")
	public ModelAndView redirectDescriptionCareerCenter(ModelAndView mv, @RequestParam(value = "idCareerCenter") Integer idCareerCenter) {
		
		CareerCenter careerCenter = ccr.findById(idCareerCenter).get();
		Profil profil = pr.findById(careerCenter.getProfil().getIdProfil()).get();
		
		mv.addObject("careerCenter", careerCenter);
		mv.addObject("profil", profil);
		
		return mv;
	}
	
	@PostMapping(value = "/viewCareerCenterBySector")
	public ModelAndView viewCareerCenterBySector(ModelAndView mv, 
			
			@RequestParam(value = "idProfil") Integer idProfil, 
			
			@RequestParam(value = "bySector") String bySector) {
		
		Profil profil = pr.findById(idProfil).get();

		mv.addObject("profil", profil);
		
		ArrayList<CareerCenter> list = (ArrayList<CareerCenter>) ccr.findAll();

		
		ArrayList<CareerCenter> careerCenterBySector = new ArrayList<CareerCenter>();
		
		for (CareerCenter careerCenter : list) {
			
			if (careerCenter.getSectorAnnonce().equals(bySector)) {
				
				careerCenterBySector.add(careerCenter);
				
			}
		}
		
		mv.addObject("careerCenterBySector", careerCenterBySector);
	
		mv.setViewName("listCareerCenter");
		
		return mv;
	}
}
