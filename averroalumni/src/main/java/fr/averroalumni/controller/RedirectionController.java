package fr.averroalumni.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RedirectionController {
	
	
	@GetMapping(value = "/inscription")
	public String inscriptionProfil() {
		
		return "InscriptionProfil";
	}
	
	@GetMapping(value = "/connexion")
	public String connexionProfil() {
		
		return "connexion";
	}
	
	
}
