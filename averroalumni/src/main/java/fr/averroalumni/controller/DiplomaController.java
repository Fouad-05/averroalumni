package fr.averroalumni.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import fr.averroalumni.beans.Account;
import fr.averroalumni.beans.Diploma;
import fr.averroalumni.beans.InformationProfil;
import fr.averroalumni.beans.Profil;
import fr.averroalumni.repositories.dao.AccountRepository;
import fr.averroalumni.repositories.dao.DiplomaRepository;
import fr.averroalumni.repositories.dao.ProfilRepository;

@Controller
public class DiplomaController {
	
	@Autowired
	DiplomaRepository diplomaRepo;
	@Autowired
	ProfilRepository profilRepo;
	@Autowired
	AccountRepository accRepo;
	
	@PostMapping(value= "/addDiplome")
	public ModelAndView addDiplome(ModelAndView mv,
			@RequestParam(value = "idAccount") Integer idAccount) {
			Account account = accRepo.findById(idAccount).get();
			
			mv.addObject("account", account);
			mv.setViewName("addDiplome");
		
		return mv;
	}
	
	@PostMapping(value = "/addDiploma")
	public ModelAndView addDiploma(ModelAndView mv, 
			/* DiPLOME */
			@RequestParam(value = "titleDiploma") String title,
			@RequestParam(value = "yearDiploma") String yearDiploma, 
			@RequestParam(value = "descDiploma") String descDiploma,
			@RequestParam(value = "linkDiploma") String linkDiploma, 
			@RequestParam(value = "school") String school,
			@RequestParam(value = "levelDiploma") String levelDiploma,
			@RequestParam(value = "typeDiploma") String typeDiploma,
			@RequestParam(value = "idAccount") Integer idAccount) {

		
		Account account = accRepo.findById(idAccount).get();
		Diploma diplome = new Diploma(title, yearDiploma, descDiploma, linkDiploma, school, levelDiploma, typeDiploma);
		
		diplome.setProfil(account.getProfil());

		diplomaRepo.save(diplome);

		diplomaRepo.flush();

		mv.addObject("account", account);
		
		ArrayList<Diploma> listDiplome = (ArrayList<Diploma>) diplomaRepo.findAll();
		
		ArrayList<Diploma> listDiplomeProfil = new ArrayList<Diploma>();
		
		for (Diploma diplomeProfil : listDiplome) {
			
			if (diplomeProfil.getProfil() == account.getProfil()) {
				listDiplomeProfil.add(diplomeProfil);
			}
		
		}
		
		mv.addObject("listDiplomeProfil", listDiplomeProfil);
		mv.setViewName("viewProfil");

		return mv;
	}
	
	@PostMapping(value = "/suppressionDiploma")
	public ModelAndView suppressionProfil(ModelAndView mv,
			/* PROFIL */
			@RequestParam(value = "idDiplome") String idDiplome,
			@RequestParam(value = "idProfil") String idProfil) {
		
		diplomaRepo.deleteById(Integer.parseInt(idDiplome));
		
		Profil profil= profilRepo.findById(Integer.parseInt(idProfil)).get();
		
		ArrayList<Diploma> listDiplome = (ArrayList<Diploma>) diplomaRepo.findAll();
		
		
		ArrayList<Diploma> listDiplomeProfil = new ArrayList<Diploma>();
		
		for (Diploma diplomeProfil : listDiplome) {
			
			if (diplomeProfil.getProfil().getIdProfil().equals(idProfil)) {
				listDiplomeProfil.add(diplomeProfil);
			}
		
		}
		
		mv.addObject("listDiplomeProfil", listDiplomeProfil);
		mv.addObject("profil", profil);
		
		  mv.setViewName("viewProfil");
		return mv;
	}
	
	@PostMapping(value = "/viewAddDiplome")
	public ModelAndView viewAddDiplome(ModelAndView mv,
			/* PROFIL */
			@RequestParam(value = "idProfil") String idProfil) {
		
		
		Profil profil= profilRepo.findById(Integer.parseInt(idProfil)).get();
		
		
		mv.addObject("Profil", profil);
		
		mv.setViewName("addDiplome");
		return mv;
	}
	
}
