package fr.averroalumni.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.averroalumni.beans.Account;
import fr.averroalumni.beans.Profil;
import fr.averroalumni.repositories.dao.AccountRepository;
import fr.averroalumni.repositories.dao.ProfilRepository;



@Controller
@SessionAttributes("account")

public class AuthentificationAccount {
	@Autowired
	AccountRepository accountRepo;
	@Autowired
	ProfilRepository profilrepo;
	
	@PostMapping(value = "/singin")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		
		Integer nbrProfil = 0;

		ArrayList<Account> listeAccount = (ArrayList<Account>) accountRepo.findAll();

		if (listeAccount != null) {
			for (Account account : listeAccount) {
				if (account.getLogin().equals(login) && BCrypt.checkpw(mdp, account.getPwd())) {
					model.addAttribute("account", account);
					
					
					System.out.println(account.getProfil().getNom());
					
					if (account.getType() == 0) {
						
						ArrayList<Profil> listeProfil = (ArrayList<Profil>) profilrepo.findAll();
						
						for (int i = 0; i < listeProfil.size(); i++) {
							nbrProfil++;
						}

						model.addAttribute("listeProfil", listeProfil);
						model.addAttribute("nbrProfil", nbrProfil);
						//mettre admindashboard
						return "Dashboard";
						
					}
					
					if (account.getType() != 0) {
						
						return "Dashboard";
					}
					
					return "menuAdmin";
				}
			}
		}

		return "home";
	}

	@GetMapping(value = "/singout")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("account");
		status.setComplete();

		return "connexion";
	}

}
