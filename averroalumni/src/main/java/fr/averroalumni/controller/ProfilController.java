package fr.averroalumni.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.averroalumni.beans.Account;
import fr.averroalumni.beans.Diploma;
import fr.averroalumni.beans.InformationProfil;
import fr.averroalumni.beans.Profil;
import fr.averroalumni.repositories.dao.DiplomaRepository;
import fr.averroalumni.repositories.dao.ProfilRepository;




@Controller
public class ProfilController {
	
	@Autowired
	ProfilRepository profilRepo;
	@Autowired
	DiplomaRepository diplomaRepo;
	
	@PostMapping(value = "/signUp")
	public ModelAndView addProfilPro(ModelAndView mv, 
			/* PROFIL */
			@RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, 
			@RequestParam(value = "mail") String mail,
			@RequestParam(value = "phoneNumber") String phoneNumber, 
			@RequestParam(value = "yearBac") String yearBac,
			/* INFORMATION PROFIL */
			@RequestParam(value = "jobTitle") String jobTitle,
			@RequestParam(value = "activitySector") String activitySector,
			@RequestParam(value = "linkJobCard") String linkJobCard,
			@RequestParam(value = "linkedin") String linkedin,
			@RequestParam(value = "linkCompany") String linkCompany,
			/* COMPTE */
			@RequestParam(value = "pwd") String pwd) {

		LocalDate dateJ = LocalDate.now();

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());
		
		Account account = new Account(mail, pwdHash, 0);
		
		Profil profil = new Profil(nom, prenom, mail, yearBac, "Ancien", dateJ);
		
		/* Si le t�l�phone est remplis a l'inscription */
		if (phoneNumber.equals("") && phoneNumber.equals(" ")) {
			profil.setPhoneNumber(phoneNumber);
		}
		
		/* Ajout du compte au profil */
		profil.setAccount(account);
		
		
		
		/* Creation de l'objet des informations profil  */
		InformationProfil infoProfil = new InformationProfil(jobTitle, activitySector);
		
		/* Si le lien de la company est remplis a l'inscription */
		if (linkCompany.equals("") && linkCompany.equals(" ")) {
			infoProfil.setLinkCompany(linkCompany);
		}
		/* Si le lien de linkedin est remplis a l'inscription */
		if (linkedin.equals("") && linkedin.equals(" ")) {
			infoProfil.setLinkedin(linkedin);
		}
		/* Si le lien du metier est remplis a l'inscription */
		if (linkJobCard.equals("") && linkJobCard.equals(" ")) {
			infoProfil.setLinkJobCard(linkJobCard);
		}
		
		profil.setInformationProfil(infoProfil);
		mv.addObject("job", infoProfil.getJobTitle());
		
		

		profilRepo.save(profil);

		profilRepo.flush();

		mv.addObject("profil", profil);

		mv.setViewName("viewProfil");

		return mv;
	}
	
	@GetMapping(value = "/listeProfil")
	public ModelAndView listeProfil(ModelAndView mv) {
		
		
		ArrayList<Profil> listProfil = (ArrayList<Profil>) profilRepo.findAll();
		
		int compteurProfil = listProfil.size();
		mv.addObject("compteurProfil", compteurProfil);
		mv.addObject("listProfil", listProfil);
		
		mv.setViewName("listeProfil");
		return mv;
	}
	
	@PostMapping(value = "/viewProfil")
	public ModelAndView viewProfil(ModelAndView mv,
			/* PROFIL */
			@RequestParam(value = "idAccount") String idAccount,
			@RequestParam(value = "idProfil") String idProfil) {
		
		
		
		Profil profil = profilRepo.findById(Integer.parseInt(idProfil)).get();
		mv.addObject("Profil", profil);
		
		
		ArrayList<Diploma> listDiplome = (ArrayList<Diploma>) diplomaRepo.findAll();
		
		ArrayList<Diploma> listDiplomeProfil = new ArrayList<Diploma>();
		
		for (Diploma diplomeProfil : listDiplome) {
			System.out.println("bug1");
			if (diplomeProfil.getProfil().getIdProfil()== Integer.parseInt(idProfil)) {
				
				listDiplomeProfil.add(diplomeProfil);
				System.out.println(diplomeProfil.getIdDiploma());
			}
		
		}
		
		mv.addObject("listDiplome", listDiplome);
		mv.addObject("listDiplomeProfil", listDiplomeProfil);
		
		
		mv.setViewName("viewProfil");
		return mv;
	}
	
	
	@GetMapping(value = "/suppressionProfil")
	public ModelAndView suppressionProfil(ModelAndView mv,
			/* PROFIL */
			@RequestParam(value = "idAccount") String idAccount,
			@RequestParam(value = "idProfil") String idProfil) {
		
		profilRepo.deleteById(Integer.parseInt(idProfil));
		
		ArrayList<Profil> listProfil = (ArrayList<Profil>) profilRepo.findAll();
		
		int compteurProfil = listProfil.size();
		mv.addObject("compteurProfil", compteurProfil);
		mv.addObject("listProfil", listProfil);
		
		  mv.setViewName("listeProfil");
		return mv;
	}
	
	@PostMapping(value = "/editProfilPro")
	public ModelAndView modifcationProfilPro(ModelAndView mv, 
			/* PROFIL */
			@RequestParam(value = "idProfil") Integer idProfil,
			@RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, 
			@RequestParam(value = "mail") String mail,
			@RequestParam(value = "phoneNumber") String phoneNumber, 
			@RequestParam(value = "yearBac") String yearBac,
			/* INFORMATION PROFIL */
			@RequestParam(value = "jobTitle") String jobTitle,
			@RequestParam(value = "activitySector") String activitySector,
			@RequestParam(value = "linkJobCard") String linkJobCard,
			@RequestParam(value = "linkedin") String linkedin,
			@RequestParam(value = "linkCompany") String linkCompany,
			/* COMPTE */
			@RequestParam(value = "pwd") String pwd,
			@RequestParam(value = "password-confirm") String pwdConfirm) {

		

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());
		String pwdHashConfirm = BCrypt.hashpw(pwdConfirm, BCrypt.gensalt());
		
		Account account = new Account(mail, pwdHash, 0);
		
		Profil profil = profilRepo.findById(idProfil).get();
		profil.setNom(nom);
		profil.setPrenom(prenom);
		profil.setMail(mail);
		profil.getInformationProfil().setJobTitle(jobTitle);
		
		/* Si le t�l�phone est remplis a l'inscription */
		if (phoneNumber.equals("") && phoneNumber.equals(" ") && profil.getPhoneNumber()!= phoneNumber) {
			profil.setPhoneNumber(phoneNumber);
		}
		
		/* Ajout du compte au profil */
		profil.setAccount(account);
		
		
		
		/* Creation de l'objet des informations profil  */
		
		
		/* Si le lien de la company est remplis a l'inscription */
		if (linkCompany.equals("") && linkCompany.equals(" ")) {
			profil.getInformationProfil().setLinkCompany(linkCompany);
		}
		/* Si le lien de linkedin est remplis a l'inscription */
		if (linkedin.equals("") && linkedin.equals(" ")) {
			profil.getInformationProfil().setLinkedin(linkedin);
		}
		/* Si le lien du metier est remplis a l'inscription */
		if (linkJobCard.equals("") && linkJobCard.equals(" ")) {
			profil.getInformationProfil().setLinkJobCard(linkJobCard);
		}
		if (pwdHash != profil.getAccount().getPwd()) {
			if (pwdHash == pwdHashConfirm) {
				profil.getAccount().setPwd(pwdHash);
			}
			
		}
		
		
		
		

		profilRepo.save(profil);

		profilRepo.flush();

		mv.addObject("profil", profil);

		mv.setViewName("listeProfil");

		return mv;
	}
	
	@PostMapping(value = "/editProfilRedirection")
	public ModelAndView editProfil(ModelAndView mv,
			/* PROFIL */
			@RequestParam(value = "idProfil") String idProfil) {
		
		
		
		Profil profil = profilRepo.findById(Integer.parseInt(idProfil)).get();
		mv.addObject("Profil", profil);
		
		  mv.setViewName("editProfil");
		return mv;
	}
	
}
