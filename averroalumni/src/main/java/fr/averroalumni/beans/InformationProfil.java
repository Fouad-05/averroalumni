package fr.averroalumni.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class InformationProfil {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idInformationProfil;
	
	@NonNull
	private String jobTitle;
	
	@NonNull
	private String activitySector;
	
	
	private String linkJobCard;
	
	
	private String linkedin;
	
	
	private String linkCompany;
	
	@OneToMany(mappedBy = "infoProfil" , cascade = CascadeType.ALL)
	private List<Competences> listCompetences;
	
	
	@OneToOne(mappedBy = "InformationProfil")
	private Profil profil;

}
