package fr.averroalumni.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class Diploma {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idDiploma;
	
	@NonNull
	private String titleDiploma;
	
	@NonNull
	private String yearDiploma;

	@NonNull
	private String descDiploma;
	
	@NonNull
	private String linkDiploma;
	
	@NonNull
	private String school;
	
	@NonNull
	private String levelDiploma;
	
	@NonNull
	private String typeDiploma;
	
	@ManyToOne
	@JoinColumn(name = "id_profil")
	private Profil profil;
}
