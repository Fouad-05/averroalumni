package fr.averroalumni.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class Profil {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idProfil;
	
	@NonNull
	private String nom;

	@NonNull
	private String prenom;
	
	@NonNull
	private String mail;
	
	
	private String phoneNumber;
	
	@NonNull
	private String yearBac;
	
	@NonNull
	private String studentClass;
	
	@NonNull
	private LocalDate dateCreationCompte;
	
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_account")
	private Account account;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_infoProfil")
	private InformationProfil InformationProfil;
	
	@OneToMany(mappedBy = "profil" , cascade = CascadeType.ALL)
	private List<Diploma> listDiploma;
	
	@OneToMany(mappedBy = "profil" , cascade = CascadeType.ALL)
	private List<CareerCenter> listCareerCenter;
	
	@OneToMany(mappedBy = "profil" , cascade = CascadeType.ALL)
	private List<CandidacyCenter> listCandidacyCenter;
	
	@OneToMany(mappedBy = "profil" , cascade = CascadeType.ALL)
	private List<Evenement> listEvent;
	
	
	
	
	
	
}
