package fr.averroalumni.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class CandidacyCenter {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCandidacyCenter;
	
	@NonNull
	private String typeCandidacy;
	
	@NonNull
	private String descCandidacy;
	
	@NonNull
	private String levelCandidacy;
	
	@NonNull
	private String durationCandidacy;
	
	@NonNull
	private String contactCandidacy;

	@NonNull
	private String companyCandidacy;

	@NonNull
	private String dateCandidacy;

	@NonNull
	private String rythmeCandidacy;

	@NonNull
	private String startCandidacy;

	@NonNull
	private String sectorCandidacy;

	@NonNull
	private String PDF;
	
	@ManyToOne
	@JoinColumn(name = "id_profil")
	private Profil profil;
}
