package fr.averroalumni.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class CareerCenter {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCareerCenter;
	
	@NonNull
	private String typeAnnonce;
	
	@NonNull
	private String descAnnonce;
	
	@NonNull
	private String levelAnnonce;
	
	@NonNull
	private String durationAnnonce;
	
	@NonNull
	private String contactAnnonce;

	@NonNull
	private String companyAnnonce;

	@NonNull
	private LocalDate dateAnnonce;

	@NonNull
	private String rythmeAnnonce;

	@NonNull
	private LocalDate startAnnonce;

	@NonNull
	private String sectorAnnonce;

	@NonNull
	private String pdf;
	
	@ManyToOne
	@JoinColumn(name = "id_profil")
	private Profil profil;
}
