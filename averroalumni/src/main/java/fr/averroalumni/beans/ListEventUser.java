package fr.averroalumni.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class ListEventUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idListEventUser;
	
	@NonNull
	private Integer idAccount;
	
	@NonNull
	private Integer numberUserPublic;
	
	@ManyToOne
	@JoinColumn(name = "id_event")
	private Evenement event;
}
