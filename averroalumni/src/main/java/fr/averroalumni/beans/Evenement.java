package fr.averroalumni.beans;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class Evenement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idEvent;
	
	@NonNull
	private String titleEvent;
	
	@NonNull
	private String descEvent;
	
	@NonNull
	private LocalDate datePostEvent;
	
	@NonNull
	private LocalDate dateEvent;
	
	@NonNull
	private String typeEvent;
	
	@NonNull
	private String placeEvent;

	@NonNull
	private Integer numberOfSitEvent;

	@NonNull
	private String contactEventOwner;

	@NonNull
	private String statusEvent;

	@NonNull
	private String animatorEvent;

	@NonNull
	private String durationEvent;

	@NonNull
	private String PDF;
	
	@NonNull
	private Integer publicPrvateEvent;
	
	@NonNull
	private String restricted;
	
	@NonNull
	private Integer freeEvent;
	
	@ManyToOne
	@JoinColumn(name = "id_profil")
	private Profil profil;
	
	@OneToMany(mappedBy = "event" , cascade = CascadeType.ALL)
	private List<ListEventUser> listEventUser;

}
